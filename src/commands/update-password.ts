import {Command} from '@oclif/command'
import {error} from '../utils/logger'

const replace = require('replace-in-file')

export default class UpdatePassword extends Command {
  static description = 'describe the command here'
  
  static args = [{name: 'encryptedPassword'}]
  
  async run() {
    const {args} = this.parse(UpdatePassword)
    
    // choco
    const shell = require('shelljs')
    if (shell.exec(`choco config set proxyPassword ${args.encryptedPassword}`).code !== 0) {
      error('\nError while setting for choco\n')
      shell.exit(1)
    }

    const filesToReplace = []

    // npmrc
    filesToReplace.push(replace({
      files: 'path.to.npmrc',
      from: 'textToFind',
      to: 'textToReplace',
    }))
  }
}
