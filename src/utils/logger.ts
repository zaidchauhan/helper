const chalk = require('chalk')

export function error(content: string | number | object) {
  console.log(chalk.bold.red(content))
}
