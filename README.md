helper
======



[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![Version](https://img.shields.io/npm/v/helper.svg)](https://npmjs.org/package/helper)
[![Downloads/week](https://img.shields.io/npm/dw/helper.svg)](https://npmjs.org/package/helper)
[![License](https://img.shields.io/npm/l/helper.svg)](https://github.com/zaidchauhan/helper/blob/master/package.json)

<!-- toc -->
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->
# Usage
<!-- usage -->
```sh-session
$ npm install -g helper
$ helper COMMAND
running command...
$ helper (-v|--version|version)
helper/0.0.0 darwin-x64 node-v10.11.0
$ helper --help [COMMAND]
USAGE
  $ helper COMMAND
...
```
<!-- usagestop -->
# Commands
<!-- commands -->
* [`helper hello [FILE]`](#helper-hello-file)
* [`helper help [COMMAND]`](#helper-help-command)
* [`helper update-password [FILE]`](#helper-update-password-file)

## `helper hello [FILE]`

describe the command here

```
USAGE
  $ helper hello [FILE]

OPTIONS
  -f, --force
  -h, --help       show CLI help
  -n, --name=name  name to print

EXAMPLE
  $ helper hello
  hello world from ./src/hello.ts!
```

_See code: [src/commands/hello.ts](https://github.com/zaidchauhan/helper/blob/v0.0.0/src/commands/hello.ts)_

## `helper help [COMMAND]`

display help for helper

```
USAGE
  $ helper help [COMMAND]

ARGUMENTS
  COMMAND  command to show help for

OPTIONS
  --all  see all commands in CLI
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v2.2.3/src/commands/help.ts)_

## `helper update-password [FILE]`

describe the command here

```
USAGE
  $ helper update-password [FILE]

OPTIONS
  -f, --force
  -h, --help       show CLI help
  -n, --name=name  name to print
```

_See code: [src/commands/update-password.ts](https://github.com/zaidchauhan/helper/blob/v0.0.0/src/commands/update-password.ts)_
<!-- commandsstop -->
